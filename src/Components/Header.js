import React, { Component } from "react";
import "./Header.css";
import logo from "./../img/logo.png";

class Header extends Component {
  render() {
    return (
      <>
        <div className="row">
          <div className="col">
            <nav className="navbar navbar-expand-lg menu navbar-light bg-light">
              <a className="navbar-brand" href="#">
                <img id="logo" src={logo} alt="Logo" />
              </a>
              <button
                className="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarNav"
                aria-controls="navbarNav"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="navbar-toggler-icon"></span>
              </button>
              <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav ml-auto mr-auto text-uppercase font-weight-bold">
                  <li className="nav-item">
                    <a className="nav-link" href="#">
                      Home
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#">
                      Bikes
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#">
                      Gear
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#">
                      Parts
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#">
                      Tires
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#">
                      Service-Info
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#">
                      Catalogues
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#">
                      Contact
                    </a>
                  </li>
                </ul>
              </div>
              <div className="menu-icons">
                <span>
                  <i className="fas fa-search mr-2"></i>
                </span>

                <span>
                  <i className="fas fa-shopping-bag"></i>
                </span>
              </div>
            </nav>
          </div>
        </div>
        <div className="row bg-light ht">
          <div className="col border-top border-bottom">
            <p className="header">Bikes</p>
          </div>
        </div>
      </>
    );
  }
}

export default Header;
