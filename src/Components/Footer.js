import React, { Component } from "react";
import "./Footer.css";

class Footer extends Component {
  render() {
    return (
      <div>
        <div class="row ht py-5 footer">
          <div class="col">
            <div class="row">
              <div class="col-3 pr-5">
                <h5 class="pb-3">Social share</h5>

                <div class="row social-icons">
                  <div class="col-3">
                    <a href="#">
                      <i class="fab fa-facebook-f"></i>
                    </a>
                  </div>
                  <div class="col-3">
                    <a href="#">
                      <i class="fab fa-instagram"></i>
                    </a>
                  </div>
                  <div class="col-3">
                    <a href="#">
                      <i class="fab fa-twitter"></i>
                    </a>
                  </div>
                  <div class="col-3">
                    <a href="#">
                      <i class="fab fa-linkedin-in"></i>
                    </a>
                  </div>
                </div>
              </div>
              <div class="col-3 bl">
                <h5 class="pb-2">Event info</h5>

                <p>
                  <a href="#">Enter now</a>
                </p>
                <p>
                  <a href="#">Event info</a>
                </p>
                <p>
                  <a href="#">Course maps</a>
                </p>
                <p>
                  <a href="#">Race pack</a>
                </p>
                <p>
                  <a href="#">Results</a>
                </p>
                <p>
                  <a href="#">FAQs</a>
                </p>
                <p>
                  <a href="#">Am I Registered?</a>
                </p>
              </div>
              <div class="col-3 bl">
                <h5 class="pb-2">Registration</h5>

                <p>
                  <a href="#">Volunteers</a>
                </p>
                <p>
                  <a href="#">Gallery</a>
                </p>
                <p>
                  <a href="#">Press</a>
                </p>
                <p>
                  <a href="#">Results</a>
                </p>
                <p>
                  <a href="#">Privacy Policy</a>
                </p>
                <p>
                  <a href="#">Service Plus</a>
                </p>
                <p>
                  <a href="#">Contacts</a>
                </p>
              </div>
              <div class="col-3 bl">
                <h5 class="pb-2">Schedule</h5>

                <p>
                  <a href="#">Gallery</a>
                </p>
                <p>
                  <a href="#">About</a>
                </p>
                <p>
                  <a href="#">Videos</a>
                </p>
                <p>
                  <a href="#">Results</a>
                </p>
                <p>
                  <a href="#">FAQs</a>
                </p>
                <p>
                  <a href="#">Results</a>
                </p>
                <p>
                  <a href="#">Volunteers</a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;
