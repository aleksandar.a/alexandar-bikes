import React, { Component } from "react";
import "./Card.css";

class Card extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cardHover: false,
    };
  }

  hoverEnter = () => {
    this.setState({
      cardHover: true,
    });
  };

  hoverLeave = () => {
    this.setState({
      cardHover: false,
    });
  };

  render() {
    return (
      <div
        onMouseEnter={this.hoverEnter}
        onMouseLeave={this.hoverLeave}
        className="card"
      >
        <img
          className={
            this.state.cardHover
              ? "card-img-top transition p-3"
              : "card-img-top p-3"
          }
          alt="Card image cap"
          src={require(`./../img/${this.props.img}.png`).default}
        />
        <div className="card-body cb-bg">
          <h6 className="card-title text-uppercase">{this.props.title}</h6>
          <p className="card-text">{this.props.price}</p>
        </div>
      </div>
    );
  }
}

export default Card;
