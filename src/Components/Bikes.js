import React, { Component } from "react";
import Filters from "./Filters";
import Card from "./Card";
import "./Bikes.css";
// import bike1 from "./img/12.png";
import axios from "axios";

export default class Bikes extends Component {
  constructor() {
    super();
    this.state = {
      imageArray: [],
      mutableImageArray: [],
    };
  }
  componentDidMount() {
    axios.get("https://json-project3.herokuapp.com/products").then((res) => {
      console.log(res.data);

      let finalArr = [];
      let lastArr = [];

      res.data.forEach((x) => {
        console.log(x.brand);

        if (finalArr.includes(x.brand)) {
          console.log("true");
        } else {
          finalArr.push(x.brand);
        }
      });

      console.log(finalArr);

      finalArr.forEach((x) => {
        let counter = 0;

        res.data.forEach((y) => {
          if (x === y.brand && counter < 2) {
            lastArr.push(y);
            counter++;
          }
        });

        counter = 0;
      });

      console.log(lastArr);

      this.setState({ imageArray: lastArr });
      this.setState({ mutableImageArray: lastArr });
    });
  }

  showMaleBikes = () => {
    let maleBikesArray = [];
    maleBikesArray = this.state.imageArray.filter((x) => {
      return x.gender === "MALE";
    });
    this.setState({
      mutableImageArray: maleBikesArray,
    });
  };

  showFemaleBikes = () => {
    let femaleBikesArray = [];
    femaleBikesArray = this.state.imageArray.filter((x) => {
      return x.gender === "FEMALE";
    });
    this.setState({
      mutableImageArray: femaleBikesArray,
    });
  };

  showLeGrandBikes = () => {
    let leGrandBikesArray = [];
    leGrandBikesArray = this.state.imageArray.filter((x) => {
      return x.brand === "LE GRAND BIKES";
    });
    this.setState({
      mutableImageArray: leGrandBikesArray,
    });
  };

  showKrossBikes = () => {
    let krossBikesArray = [];
    krossBikesArray = this.state.imageArray.filter((x) => {
      return x.brand === "KROSS";
    });
    this.setState({
      mutableImageArray: krossBikesArray,
    });
  };

  showExplorerBikes = () => {
    let explorerBikesArray = [];
    explorerBikesArray = this.state.imageArray.filter((x) => {
      return x.brand === "EXPLORER";
    });
    this.setState({
      mutableImageArray: explorerBikesArray,
    });
  };

  showVisitorBikes = () => {
    let visitorBikesArray = [];
    visitorBikesArray = this.state.imageArray.filter((x) => {
      return x.brand === "VISITOR";
    });
    this.setState({
      mutableImageArray: visitorBikesArray,
    });
  };

  showPonyBikes = () => {
    let ponyBikesArray = [];
    ponyBikesArray = this.state.imageArray.filter((x) => {
      return x.brand === "PONY";
    });
    this.setState({
      mutableImageArray: ponyBikesArray,
    });
  };

  showForceBikes = () => {
    let forceBikesArray = [];
    forceBikesArray = this.state.imageArray.filter((x) => {
      return x.brand === "FORCE";
    });
    this.setState({
      mutableImageArray: forceBikesArray,
    });
  };

  showEbikes = () => {
    let eBikesArray = [];
    eBikesArray = this.state.imageArray.filter((x) => {
      return x.brand === "E-BIKES";
    });
    this.setState({
      mutableImageArray: eBikesArray,
    });
  };

  showIdealBikes = () => {
    let idealBikesArray = [];
    idealBikesArray = this.state.imageArray.filter((x) => {
      return x.brand === "IDEAL";
    });
    this.setState({
      mutableImageArray: idealBikesArray,
    });
  };

  showAllBikes = () => {
    this.setState({
      mutableImageArray: this.state.imageArray,
    });
  };

  render() {
    return (
      <div className="row filter bg-light">
        <div className="col-3 cursor">
          <Filters
            renderAllBikes={this.showAllBikes}
            bikesCount={this.state.imageArray.length}
            maleBikes={
              this.state.imageArray.filter((x) => {
                return x.gender === "MALE";
              }).length
            }
            renderMaleBikes={this.showMaleBikes}
            femaleBikes={
              this.state.imageArray.filter((x) => {
                return x.gender === "FEMALE";
              }).length
            }
            renderFemaleBikes={this.showFemaleBikes}
            leGrandBikes={
              this.state.imageArray.filter((x) => {
                return x.brand === "LE GRAND BIKES";
              }).length
            }
            renderLeGrandBikes={this.showLeGrandBikes}
            krossBikes={
              this.state.imageArray.filter((x) => {
                return x.brand === "KROSS";
              }).length
            }
            renderKrossBikes={this.showKrossBikes}
            explorerBikes={
              this.state.imageArray.filter((x) => {
                return x.brand === "EXPLORER";
              }).length
            }
            renderExplorerBikes={this.showExplorerBikes}
            visitorBikes={
              this.state.imageArray.filter((x) => {
                return x.brand === "VISITOR";
              }).length
            }
            renderVisitorBikes={this.showVisitorBikes}
            ponyBikes={
              this.state.imageArray.filter((x) => {
                return x.brand === "PONY";
              }).length
            }
            renderPonyBikes={this.showPonyBikes}
            forceBikes={
              this.state.imageArray.filter((x) => {
                return x.brand === "FORCE";
              }).length
            }
            renderForceBikes={this.showForceBikes}
            eBikes={
              this.state.imageArray.filter((x) => {
                return x.brand === "E-BIKES";
              }).length
            }
            renderEbikes={this.showEbikes}
            idealBikes={
              this.state.imageArray.filter((x) => {
                return x.brand === "IDEAL";
              }).length
            }
            renderIdealBikes={this.showIdealBikes}
          />
        </div>
        <div className="col-9 bikes pr-5">
          {this.state.mutableImageArray.map((element, i) => (
            <Card
              img={element.image}
              title={element.name}
              price={`${element.price} $`}
              key={i}
              id={i.id}
              name={element.name}
            />
          ))}
        </div>
      </div>
    );
  }
}
