import React, { Component } from "react";
import "./Filters.css";

class Filters extends Component {
  state = {
    activeFilter: "showall",
  };

  setActiveFilter = (filterName) => {
    this.setState({ activeFilter: filterName });
  };

  render() {
    return (
      <>
        <h3 className="font-weight-bold">Filter by:</h3>
        <div
          className="row"
          onClick={(e) => {
            this.setActiveFilter("showall");
            this.props.renderAllBikes();
          }}
        >
          <div className="col-9 py-4 bb1">
            <span
              className={
                this.state.activeFilter === "showall"
                  ? "showAll activated"
                  : "showAll"
              }
            >
              Show all
            </span>
          </div>
          <div className="col-3 py-4 bb2">
            <span
              className={
                this.state.activeFilter === "showall"
                  ? "badge activatedBadge"
                  : "badge"
              }
            >
              {this.props.bikesCount}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col gender">
            <h4 className="font-weight-bold pb-2">Gender</h4>
            <div
              className="row"
              onClick={(e) => {
                this.setActiveFilter("male");
                this.props.renderMaleBikes();
              }}
            >
              <div className="col-9">
                <span
                  className={
                    this.state.activeFilter === "male" ? "activated" : ""
                  }
                >
                  Male
                </span>
              </div>
              <div className="col-3 pb-3">
                <span
                  className={
                    this.state.activeFilter === "male"
                      ? "badge activatedBadge"
                      : "badge"
                  }
                >
                  {this.props.maleBikes}
                </span>
              </div>
            </div>
            <div
              onClick={(e) => {
                this.setActiveFilter("female");
                this.props.renderFemaleBikes();
              }}
              className="row"
            >
              <div className="col-9 bb1">
                <span
                  className={
                    this.state.activeFilter === "female" ? "activated" : ""
                  }
                >
                  Female
                </span>
              </div>
              <div className="col-3 pb-3 bb2">
                <span
                  className={
                    this.state.activeFilter === "female"
                      ? "badge activatedBadge"
                      : "badge"
                  }
                >
                  {this.props.femaleBikes}
                </span>
              </div>
            </div>
          </div>
        </div>
        <div className="row pb-5">
          <div className="col brand">
            <h4 className="font-weight-bold pb-2">Brand</h4>
            <div
              onClick={(e) => {
                this.setActiveFilter("legrand");
                this.props.renderLeGrandBikes();
              }}
              className="row pb-2"
            >
              <div className="col-9">
                <span
                  className={
                    this.state.activeFilter === "legrand" ? "activated" : ""
                  }
                >
                  Le grand bikes
                </span>
              </div>
              <div className="col-3">
                <span
                  className={
                    this.state.activeFilter === "legrand"
                      ? "badge activatedBadge"
                      : "badge"
                  }
                >
                  {this.props.leGrandBikes}
                </span>
              </div>
            </div>
            <div
              onClick={(e) => {
                this.setActiveFilter("kross");
                this.props.renderKrossBikes();
              }}
              className="row  pb-2"
            >
              <div className="col-9">
                <span
                  className={
                    this.state.activeFilter === "kross" ? "activated" : ""
                  }
                >
                  Kross
                </span>
              </div>
              <div className="col-3">
                <span
                  className={
                    this.state.activeFilter === "kross"
                      ? "badge activatedBadge"
                      : "badge"
                  }
                >
                  {this.props.krossBikes}
                </span>
              </div>
            </div>
            <div
              onClick={(e) => {
                this.setActiveFilter("explorer");
                this.props.renderExplorerBikes();
              }}
              className="row  pb-2"
            >
              <div className="col-9">
                <span
                  className={
                    this.state.activeFilter === "explorer" ? "activated" : ""
                  }
                >
                  Explorer
                </span>
              </div>
              <div className="col-3">
                <span
                  className={
                    this.state.activeFilter === "explorer"
                      ? "badge activatedBadge"
                      : "badge"
                  }
                >
                  {this.props.explorerBikes}
                </span>
              </div>
            </div>
            <div
              onClick={(e) => {
                this.setActiveFilter("visitor");
                this.props.renderVisitorBikes();
              }}
              className="row  pb-2"
            >
              <div className="col-9">
                <span
                  className={
                    this.state.activeFilter === "visitor" ? "activated" : ""
                  }
                >
                  Visitor
                </span>
              </div>
              <div className="col-3">
                <span
                  className={
                    this.state.activeFilter === "visitor"
                      ? "badge activatedBadge"
                      : "badge"
                  }
                >
                  {this.props.visitorBikes}
                </span>
              </div>
            </div>
            <div
              onClick={(e) => {
                this.setActiveFilter("pony");
                this.props.renderPonyBikes();
              }}
              className="row  pb-2"
            >
              <div className="col-9">
                <span
                  className={
                    this.state.activeFilter === "pony" ? "activated" : ""
                  }
                >
                  Pony
                </span>
              </div>
              <div className="col-3">
                <span
                  className={
                    this.state.activeFilter === "pony"
                      ? "badge activatedBadge"
                      : "badge"
                  }
                >
                  {this.props.ponyBikes}
                </span>
              </div>
            </div>
            <div
              onClick={(e) => {
                this.setActiveFilter("force");
                this.props.renderForceBikes();
              }}
              className="row  pb-2"
            >
              <div className="col-9">
                <span
                  className={
                    this.state.activeFilter === "force" ? "activated" : ""
                  }
                >
                  Force
                </span>
              </div>
              <div className="col-3">
                <span
                  className={
                    this.state.activeFilter === "force"
                      ? "badge activatedBadge"
                      : "badge"
                  }
                >
                  {this.props.forceBikes}
                </span>
              </div>
            </div>
            <div
              onClick={(e) => {
                this.setActiveFilter("ebikes");
                this.props.renderEbikes();
              }}
              className="row  pb-2"
            >
              <div className="col-9">
                <span
                  className={
                    this.state.activeFilter === "ebikes" ? "activated" : ""
                  }
                >
                  E-Bikes
                </span>
              </div>
              <div className="col-3">
                <span
                  className={
                    this.state.activeFilter === "ebikes"
                      ? "badge activatedBadge"
                      : "badge"
                  }
                >
                  {this.props.eBikes}
                </span>
              </div>
            </div>
            <div
              onClick={(e) => {
                this.setActiveFilter("ideal");
                this.props.renderIdealBikes();
              }}
              className="row "
            >
              <div className="col-9">
                <span
                  className={
                    this.state.activeFilter === "ideal" ? "activated" : ""
                  }
                >
                  Ideal
                </span>
              </div>
              <div className="col-3">
                <span
                  className={
                    this.state.activeFilter === "ideal"
                      ? "badge activatedBadge"
                      : "badge"
                  }
                >
                  {this.props.idealBikes}
                </span>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Filters;
