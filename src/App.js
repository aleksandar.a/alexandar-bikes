import React from "react";
import Bikes from "./Components/Bikes";
import Footer from "./Components/Footer";
import Header from "./Components/Header";

function App() {
  return (
    <div className="container">
      <Header />
      <Bikes />
      <Footer />
    </div>
  );
}

export default App;
